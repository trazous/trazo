<!doctype html>
<html lang="ES">

<head>
	<title>Trazo | Publicidad, Diseño e Impresión</title>
	<meta name="Description" content="Somos un despacho de diseño,impresión, rotulación y diseño de paginas web.  Ubicado en Calle Hércules 2473, tenemos 20 años de experiencia en impresión de catálogos, folletos, papelería comercial, formatos comerciales, diseño de logotipos, diseño e impresión de libros, rotulación de oficinas y vehículos.">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="keywords" content="publicidad, imprenta, imagen corporativa, rotulación de oficinas, cuadros decorativos, branding, rotulación de vehículos, diseño e impresión de carpetas, diseño e impresión de libros, diseño e impresión de revistas, impresos" />
	<meta charset="utf-8" />

	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="css/base.css" />
	<link rel="stylesheet" type="text/css" href="css/media.queries.css" />
	<link rel="stylesheet" type="text/css" href="css/tipsy.css" />
	<link rel="stylesheet" type="text/css" href="css/flexslider.css" />
	<link rel="stylesheet" type="text/css" href="css/gallery.css" />
	<link rel="stylesheet" type="text/css" href="css/contact-style.css" />
	<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css" />
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Varela+Round' />
	<link rel="icon" type="image/png" href="favicon.ico" />

	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Orbitron' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Libre+Baskerville:400,400italic' rel='stylesheet' type='text/css'>
	
	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.html" />
	<link rel="apple-touch-icon" href="images/apple-touch-icon.html">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

	<!-- Javascripts -->
	<script type="text/javascript" src="js/modernizr.custom.26584.js"></script> 
	<script type="text/javascript" src="js/swfobject.js"></script> 
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="js/html5shiv.js"></script>
	<script type="text/javascript" src="js/jquery.tipsy.js"></script>
	<script type="text/javascript" src="js/jquery.mobilemenu.js"></script>
	<script type="text/javascript" src="js/jquery.infieldlabel.js"></script>
	<script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<script type="text/javascript" src="js/fancybox/jquery.easing-1.3.pack.js"></script>
	<script type="text/javascript" src="js/jquery.css-rorate-scale.js"></script>
	<script type="text/javascript" src="js/jquery.quicksand.js"></script>
	<script type="text/javascript" src="js/jquery.twitter.js"></script>
	<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
	<script type="text/javascript" src="js/jquery.ui.totop.min.js"></script>
	<script type="text/javascript" src="js/jquery.nav.min.js"></script>
	<script type="text/javascript" src="js/scrolio.js"></script>
	<script type="text/javascript" src="js/jquery.miniColors.js"></script>
	<script type="text/javascript" src="js/demo.js"></script>
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="/js/script.js"></script>	
</head>

<body class="linen">
<img id="bg" src="images/backgrounds/blue.jpg">	
	<div id="page_wrapper">
		<header>
		<div class="container">
			<nav>
				<ul>
					<li class="current">
						<a href="#intro">Home</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="#about">Nosotros</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="#portfolio">Portafolio</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="#services">Servicios</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="http://trazo.us/video.html">Video</a>
						<span class="divider">/</span>
					</li>
					<li>
						<a href="#contact">Contacto</a>
						<span class="divider">/</span>
					</li>
                    <li>
					<strong style="font-size: 16px; color: #F6A814;">Tel: (33)1522-1066 <span style="color: #6F6D6A; font-size: 14px;">whatsapp</span> 333.167.8104</strong> </li>
					
					
					
					<a href="https://api.whatsapp.com/send?phone=523331678104&text=Hola"><img src="images/whatsapp.png"></a>
				</ul>
			</nav>
		<!-- Start Social Icons -->
			<aside>
			<ul class="social">
			<li class="facebook"><a href="https://www.facebook.com/pages/Trazo-Diseño/182151075138282">Facebook</a></li>
			<li class="twitter"><a href="https://twitter.com/impresiontrazo">Twitter</a></li>
			<li class="email"><a href="http://www.youtube.com/user/trazo2006" title="Youtube">Youtube</a></li>
			<li class="google"><a href="https://plus.google.com/106288043318619685090" rel="publisher">Google+</a></li>
		<!--<li class="dribbble"><a href="">Dribbble</a></li>
			<li class="flickr"><a href="">Flickr</a></li>-->
			</ul>
			</aside>
		</div>
		</header>
		<!-- End Header -->
	
		<!-- Start Intro Section -->
		<section id="intro" class="container">
			
		<!-- Start Logo -->
		<div class="logo"> <!-- Use class: "centered" for a centered logo -->
		<img src="images/trazo-logo.png" class="centered" alt="logo"  />
		</div>

		<!-- Start Slider -->
		<div class="flexslider-container">
			<div id="slider" class="flexslider">
				<ul class="slides">
				
				
					<li> <img src="images/slider/impresion-de-formatos-para-negocios.jpg" alt="impresión de formatos para negocios" /></li>
					<li> <img src="images/slider/menu-de-servicios.jpg" alt="nuestros servicios" /></li>
					<li> <img src="images/slider/viniles-decorativos-para-oficinas.jpg" alt="viniles" /></li>
					<li> <img src="images/slider/diseno-de-logotipos.jpg" alt="diseño de logotipos" /></li>
					<li> <img src="images/slider/impresion-de-papeleria-comercial.jpg" alt="impresión de papelería comercial" /></li>
					<li> <img src="images/slider/impresion-de-etiquetas.jpg" alt="impresión de etiquetas" /></li>
					<li> <img src="images/slider/diseno-de-etiquetas-para-producto.jpg" alt="diseño de etiquetas para producto" /></li>
					<li> <img src="images/slider/diseno-de-menus-para-restaurantes.jpg" alt="diseño de menus para restaurantes" /></li>
					<li> <img src="images/slider/diseno-de-empaques.jpg" alt="diseño de empaques" /></li>
					<li> <img src="images/slider/diseno-de-folletos.jpg" alt="diseño e impresión de folletos" /></li>
					<li> <img src="images/slider/diseno-de-displays.jpg" alt="diseño de displays" /></li>
					<li> <img src="images/slider/diseno-de-catalogos.jpg" alt="diseño e impresión de catalogos" /></li>
					<li> <img src="images/slider/viniles-para-cuartos-de-bebe.jpg" alt="viniles decorativos para cuartos de bebe" /></li>
					<li> <img src="images/slider/diseno-e-impresion-de-libros.jpg" alt="diseño e impresión de libros y publicaciones" /></li>
				</ul>
			</div>
		</div>
					
		</section>
		<!-- End Intro Section -->

		
		<!-- Start Pages Container -->
		<section id="pages" class="container">

		<!-- Start CSS MENU -->
		<h1 id="description">Publicidad, Impresión, Rotulación y Diseño a su medida</h1>
		<div id="about" class="page">
		<div id="main_menu">
<a href="#current" style="cursor: hand" id="home_btn" class="tTip" title="Inicio"> <span class="icon"></span></a>
<a href="http://trazo.us/vinyl.html" style="cursor: hand" id="services_btn" class="tTip" title="Viniles"><span class="icon"></span> </a>
<a href="#portfolio" style="cursor: hand" id="portfolio_btn" class="tTip" title="Portafolio de Diseño"><span class="icon"></span> </a>
<a href="#services" style="cursor: hand" id="web_btn" class="tTip" title="Desarrollo Web"><span class="icon"></span> </a>
<a href="#contact" style="cursor: hand" id="contact_btn" class="tTip" title="Contacto"> <span class="icon" ></span> </a>
		</div>
		</div>	

		<div id="about" class="page">
			<div class="heading"> <h2>Servicios</h2> </div>
			
			<!--Start Services Bloq -->
			<div class="bloq1">
			<img src="images/icons/diseno-de-paginas-web.png" alt="Diseño de paginas web en Guadalajara" id="bloq-left" title="Diseño de paginas web">
			<h3> <span style="font-size: 26px"><strong style="font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif">Diseño de Paginas Web</strong></span><strong style="font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif"></strong></h3>
			<p id="bloq-text">Si quieres vender más, tener un vendedor activo las 24 horas del día, que tus productos se muestren en todo el mundo, aumentar tus ingresos.<br>
			  Sin duda esto lo puedes lograr teniendo un atractivo sitio web, Nosotros diseñamos y desarrollamos paginas web creativas que hacen la diferencia.</p>
			</div>
			<!--Start Services Bloq -->
			
			<!--Start Services Bloq -->
			<div class="bloq2">
			<img src="images/icons/diseno-grafico-imprenta.png" alt="Diseño grafico  impresion" id="bloq-right" title="Diseño grafico imprenta">
			<h3><strong style="font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 26px;">Diseño Gráfico e Imprenta</strong></h3>
			<p id="bloq-text">El diseño gráfico es la herramienta maestra que hace de las marcas iconos. Creamos; logotipos, imprenta, carteles, serigrafía, impresión digital, folletos, tarjetas de presentación, catalogo, imagen corporativa, volantes, papelería, etiquetas, rótulos y más..</p>
		
			
			</div>
			<!--Start Services Bloq -->
		</div>

		<div id="about" class="page">
			<div class="heading">
				<h2><strong style="font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 36px;">Misión y Visión</strong></h2>
				<span>Somos un despacho de diseño e impresión fundado en 1995.</span>
			</div>
		<div class="content">
					
		<!-- Start Bio -->
		<div class="bio">
			<p style="font-size: 1.3em;">
			Ser el mejor proveedor de servicios de impresión y publicidad, resolviendo de manera eficaz y con rapidez sus necesidades de impresión, publicidad, rotulación. Contribuyendo así en el éxito de las empresas mexicanas y empresas transnacionales.</p>
		</div>

		<!-- Start Tweets -->
		<div class="box tweets" style="display: none;">
		<span class="icon social"><a href="http://twitter.com/">d</a></span>
			<div class="flexslider-container">
				<div id="twitter_stream" class="flexslider"></div>
			</div>
		</div>
		</div>
		</div>
			
		<!-- Start Portfolio -->
		<div id="portfolio" class="page">
			<div class="heading">
				<h2><strong>Portafolio</strong></h2><span></span>
			</div>
		
			<div class="content">
					
			<!-- Start Portfolio Filters -->
			<ul class="filter_list">
				<li><a href="javascript:;" class="print">Diseño</a></li>
				<li><a href="javascript:;" class="mobile">Logotipos</a></li>
				<li><a href="javascript:;" class="web">Web</a></li>
				</li>
				<!--<li><a href="javascript:;" class="logo">Impreso</a></li>-->
				<!--<li class="current"><a href="javascript:;" class="all-items">Todo</a></li> -->
			</ul>
					
			<!-- Start Portfolio Items -->
				<ul class="filter_items">
				<li data-type="print">
				<a href="images/content/diseno de volantes  para restaurante.jpg" class="fancybox"><img src="images/content/mini-diseno-de volantes-para restaurante.jpg" alt="Diseño de volantes para restaurante
" title="Diseño de volantes para restaurante " /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseno de  menus.jpg" class="fancybox"><img src="images/content/mini-diseno-de-menus.jpg" alt="Diseño de menus" title="Diseño de menus" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseno de banners.jpg" class="fancybox"><img src="images/content/mini-diseno-de-banners.jpg" alt="Diseño de Banners publicitarios" title="Duseño de banners publicitarios" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño de lonas para esteticas.jpg" class="fancybox"><img src="images/content/mini-diseño-de-lonas.jpg" alt="Diseño de lonas" title="Diseño de lonas" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño e impresión de tripticos.jpg" class="fancybox"><img src="images/content/mini-diseño-e impresion-de-tripticos.jpg" alt="Impresion de tripticos" title="Impresion de tripticos" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño de empaques.jpg" class="fancybox"><img src="images/content/mini-diseño-de-empaques.jpg" alt="Diseño de empaques" title="diseño de empaques" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/impresion de folletos.jpg" class="fancybox"><img src="images/content/mini-impresion-de-folletos.jpg" alt="Impresion de folletos" title="Impresion de folletos" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño de empaques.jpg" class="fancybox"><img src="images/content/mini-diseño-de-empaques.jpg" alt="Diseño de empaques" title="Diseño de empaques" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño e impresion de etiquetas.jpg" class="fancybox"><img src="images/content/mini-diseño-e impresion-de -etiquetas.jpg" alt="Diseño e impresion de etiquetas" title="Diseño e impresion de etiquetas" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño de etiquetas.jpg" class="fancybox"><img src="images/content/mini-diseño-de-etiquetas.jpg" alt="diseño de etiquetas" title="diseño de etiquetas" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño e impresion de etiquetas.jpg" class="fancybox"><img src="images/content/mini-impresion-y-diseño-de-etiquetas.jpg" alt="Impresion y diseño de etiquetas" title="Impresion y diseño de etiquetas" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño de etiqueta para carne.jpg" class="fancybox"><img src="images/content/mini-diseño-de-etiqueta-para-carne.jpg" alt="Diseño de etriqueta para carne" title="Diseño de etiqueta para carne" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño e impresión de etiquetas.jpg" class="fancybox"><img src="images/content/mini-diseño-e-impresión-de-etiquetas.jpg" alt="Diseño e impresion de etiqeutas" title="Diseño e impresion de etiquetas" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño de display.jpg" class="fancybox"><img src="images/content/mini-diseño-de-display.jpg" alt="Dsieño de display" title="Diseño de display" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/impresion de folletos para empresas.jpg" class="fancybox"><img src="images/content/mini-impresion-de-folletos-para-empresas.jpg" alt="Impresion de folletos para empresas" title="Impresion de folletos para empresas" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño de folletos turisticos.jpg" class="fancybox"><img src="images/content/mini-diseño-de-folletos-turisticos.jpg" alt="Diseño de folletos turisticos" title="Diseño de folletos turisticos" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño de revistas.jpg" class="fancybox"><img src="images/content/mini-diseño-de-revistas.jpg" alt="diseño de revistas" title="diseño de revistas" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/diseño e impresion de folletos.jpg" class="fancybox"><img src="images/content/mini-diseño-e impresion-de folletos-turisticos.jpg" alt="Diseño e impresion de folletos turisticos" title="Diseño e impresion de folletos turisticos" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/Diseño de catalogos.jpg" class="fancybox"><img src="images/content/mini-Diseño-de-catalogos.jpg" alt="Diseño de catalogos" title="Diseño de catalogos" /></a>
				</li>
				<li data-type="print">
				<a href="images/content/Diseño e impresion de catalogos.jpg" class="fancybox"><img src="images/content/mini-Diseño-e-impresion-de-catalogos.jpg" alt="Diseño e impresion de catalogos" title="Diseño e impresion de catalogos" /></a>
				</li>
				<li data-type="web">
				<a href="images/content/notaria_large.jpg" class="fancybox"><img src="images/content/notaria_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="web">
				<a href="images/content/huichol_large.jpg" class="fancybox"><img src="images/content/huichol_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="web">
				<a href="images/content/nouvelle_large.jpg" class="fancybox"><img src="images/content/nouvelle_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="web">
				<a href="images/content/hypnocenter_large.jpg" class="fancybox"><img src="images/content/hypnocenter_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logotexas_large.jpg" class="fancybox"><img src="images/content/logotexas_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logosand_large.jpg" class="fancybox"><img src="images/content/logosand_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logoinglaterra_large.jpg" class="fancybox"><img src="images/content/logoinglaterra_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logomedisales_large.jpg" class="fancybox"><img src="images/content/logomedisales_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logonitro_large.jpg" class="fancybox"><img src="images/content/logonitro_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logocarne_large.jpg" class="fancybox"><img src="images/content/logocarne_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logo10anos_large.jpg" class="fancybox"><img src="images/content/logo10anos_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logocarneaustralia_large.jpg" class="fancybox"><img src="images/content/logocarneaustralia_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logocasaarrayan_large.jpg" class="fancybox"><img src="images/content/logocasaarrayan_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logoamigosgranja_large.jpg" class="fancybox"><img src="images/content/logoamigosgranja_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logovaldez_large.jpg" class="fancybox"><img src="images/content/logovaldez_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logokaesa_large.jpg" class="fancybox"><img src="images/content/logokaesa_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logoarrachera_large.jpg" class="fancybox"><img src="images/content/logoarrachera_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/logolaboratorios_large.jpg" class="fancybox"><img src="images/content/logocosme_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/berries_large.jpg" class="fancybox"><img src="images/content/berries_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/biopower_large.jpg" class="fancybox"><img src="images/content/biopower_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/casa_large.jpg" class="fancybox"><img src="images/content/casa_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/MAC_large.jpg" class="fancybox"><img src="images/content/mac_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/pedicare_large.jpg" class="fancybox"><img src="images/content/pedicare_thumb.jpg" alt="" /></a>
				</li>
				<li data-type="mobile">
				<a href="images/content/lomeli_large.jpg" class="fancybox"><img src="images/content/lomeli_thumb.jpg" alt="" /></a>
				</li>
				</ul>
			</div>
		</div>
			
			<!-- Start Services -->
			<div id="services" class="page">
				<div class="heading">
					<h2>Clientes</h2>
					<span></span>
				</div>
				<div class="content">
					<p style="font-size: 1.3em;">A lo largo de más de 20 años estos son algunos de los clientes a quienes hemos tenido la oportunidad de ayudar con nuestros servicios de impresión, publicidad, rotulación de vehículos y oficina y sitios web.</p>
					<br/>
					
				<!-- Start Services Row -->
					<div id="navcontainer">
           				<ul id="navlist">
							<li class="list_blocks"> <img src="images/icons/logo_11.png" width="120" height="75"/> </li>
           					<li class="list_blocks"> <img src="images/icons/logo_12.png" width="120" height="75"/> </li>
           					<li class="list_blocks"> <img src="images/icons/logo_13.png" width="120" height="75"/> </li> 
            				<li class="list_blocks"> <img src="images/icons/logo_14.png" width="120" height="75"/> </li>
           					<li class="list_blocks"> <img src="images/icons/logo_15.png" width="120" height="75"/> </li>
           				</ul>
						<ul id="navlist">
							<li class="list_blocks"> <img src="images/icons/logo_1.png" width="120" height="75"/> </li>
            				<li class="list_blocks"> <img src="images/icons/logo_2.png" width="120" height="75"/> </li>
           					<li class="list_blocks"> <img src="images/icons/logo_3.png" width="120" height="75"/> </li>
           					<li class="list_blocks"> <img src="images/icons/logo_4.png" width="120" height="75"/> </li>
           					<li class="list_blocks"> <img src="images/icons/logo_5.png" width="120" height="75"/> </li>
						</ul>
						<ul id="navlist">
							<li class="list_blocks"> <img src="images/icons/logo_6.png" width="120" height="75"/> </li>
           					<li class="list_blocks"> <img src="images/icons/logo_7.png" width="120" height="75"/> </li>
           					<li class="list_blocks"> <img src="images/icons/logo_8.png" width="120" height="75"/> </li> 
            				<li class="list_blocks"> <img src="images/icons/logo_9.png" width="120" height="75"/> </li>
           					<li class="list_blocks"> <img src="images/icons/logo_10.png" width="120" height="75"/> </li>
           				</ul>
					</div>
				<!-- End Services Row -->	
				</div>
			</div>
			<!-- End Services -->
				
			<!-- Start Contact -->
			<div id="contact" class="page">
				<div class="heading">
					<h2>Contacto</h2>
					<span></span>
				</div>
				<div class="content">
					<p style="font-size: 1.3em;">Envienos un correo electronico un representante se pondra en contacto con usted.</p>
					<br/>
					
					<!-- Start Contact Form -->
					<div id="contact_form">
						<div id='form'>
                        <!-- AJGR 01-jun-2016 envio de datos a script lanza correo www.diprosis.mx -->
						<form> 
			
				<input type='text' name='name' placeholder='Name *' />
				<label class='error' id='name_error'>This field is required.</label>
				
				<input type='text' name='email' placeholder='Email *' />
				<label class='error' id='email_error'>This field is required.</label>
				
				<textarea placeholder='Your message *' name='message'></textarea>
				<label class='error' id='message_error'>This field is required.</label>
				
				<input type='hidden' name='ip' value='<?php echo $_SERVER['REMOTE_ADDR'] ?>'/>
				<input type='submit' value='send' class='submit' />
				
				<span>Fields marked with * are required.</span>
				<span class='ip'>Your IP "<b><?php echo $_SERVER['REMOTE_ADDR'] ?></b>" will be logged for safety reasons.</span>
				
					</form>
					</div>
						
					</div>
					<!-- End Contact Form -->
					
					<!-- Start Google Map -->
					<div id="map">
						<address>
							<div class="one_half">
								<b>Trazo Diseño</b> <br/>
								Hércules 2473 Jardines del Bosque, Guadalajara, Jaisco, México.
							</div>
							<div class="one_half column_last">
								<span class="icon general">r</span> (33) 1522-1066 <br/>
								<span class="icon social">r</span> Trazo Diseño <br/>
								<span class="icon general">h</span> agustin@trazo.us
							</div>
						</address>
						
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3733.132117023855!2d-103.38193238559863!3d20.664203305500497!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8428ae0a8dff8eab%3A0x94ec82d33eae09a1!2sCalle+H%C3%A9rcules+2473%2C+Jardines+del+Bosque%2C+44520+Guadalajara%2C+Jal.!5e0!3m2!1ses!2smx!4v1554485881722!5m2!1ses!2smx" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>>						
                <div>
							<img src="images/contact.png" style="padding-left: 5%;">
						</div>
							
					</div>
					<!-- End Google Map -->
					
				</div>
			</div>
			<!-- End Contact -->
			
		</section>
		<!-- End Pages Container -->

		<!-- Start Footer -->
		<footer class="container">
			<p>Trazo Diseño &copy; 2019. Todos los derechos reservados.</p>
		</footer>
		<!-- End Footer -->

	</div>
	<!-- End Wrapper -->
 <script type="text/javascript" src="js/jquery.gallery.js"></script>
        <script type="text/javascript">
            $(function() {
                $('#dg-container').gallery({
                    autoplay    :   true
                });
            });
        </script>

        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42624426-1', 'trazo.us');
  ga('send', 'pageview');

</script>
</body>
</html>